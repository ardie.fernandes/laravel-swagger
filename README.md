## Installation

#### Composer
- Map the GitLab repository in the laravel project's `composer.json` file
```
"repositories": [
    {
        "type": "vcs",
        "url": "git@gitlab.com:ardie.fernandes/laravel-swagger.git"
    }
],
```
- Run the commands
```console
# composer require ardie.fernandes/laravel-swagger

# php artisan vendor:publish --provider "LaravelSwagger\LaravelSwaggerServiceProvider"
```
<br>

## Configuration

#### .env file
- **APP_NAME**<br>
    Name to be shown as title for the API Documentation page.
    Might be used aswell as main label at the API Documentation page when used along side with the [Info annotation example](#info_example_1).
    ```
    Allowed value: String
    Optional: true
    Default: Laravel Swagger
    ```
- **APP_ENV**<br>
    Project enviroment variable used to automatically enable or disable access to the API Documentation routes
    ```
    Allowed value: String
    Optional: true
    Default: dev
    ```
- **APP_VERSION**<br>
    API project version number to be shown with the main label at the API Documentation page when used along side with the [Info annotation example](#info_example_1).
    ```
    Allowed value: String
    Optional: true
    Default: 0.1
    ```
- **APIDOC_ENABLE**<br>
    Optional flag which can be added to the .env file to override the API Documentation routes enable flag based on the APP_ENV, as long as the `enable` configuration itself is not changed.
    ```
    Allowed value: Boolean
    Optional: true
    Default: false
    ```
<br>

#### app/config/swagger.php file
- **enable**<br>
    Enables application access to API documentation routes based on
    the APP_ENV environment configuration or the APIDOC_ENABLE environment
    configuration.
    ```
    Allowed value: Boolean, NULL
    Optional: true
    Default: true for APP_ENV development and testing, false for production
    ```
- **path**<br>
    The `path` configuration allows you to define the absolute path to the
    folders and files to be scanned by the OpenAPI processors in search of
    the supported annotations. The `path` configuration can either be a string or an array of strings,
    which in any case will reffer to the absolute path of the folders and
    files to be scanned.<br>
    Any other value type for the `path` configuration will set the OpenAPI
    processors to scan the defaul folder app/Http/Controllers.
    ```
    Allowed value: String, Array of strings, NULL
    Optional: true
    Default: app/Http/Controllers
    ```
- **middleware**<br>
    As in any other Laravel router configuration, the `middleware`
    configuration can be either a string or an array of strings.
    The `middleware` configuration as a string, or an array of strings, can
    either contain:
    - The qualified class name, or an array of qualified classes names,
    - An alias, or array of aliases (in both cases previously defined in the
      App\Http\Kernel class),
    - A mix of both.<br>

    In any case the middlewares will be aplied in the defined order. If null, or any other value type, requests to the API documentation
    routes will not pass through custom middleware handling.<br>
    For more information about Middlewares, visit the link below:
    [https://laravel.com/docs/8.x/middleware](https://laravel.com/docs/8.x/middleware).
    ```
    Allowed value: String, Array of strings, NULL
    Optional: true
    Default: no default
    ```
- **filter**<br>
    The `filter` configuration allows the processor to filter the `operations`
    returned by the `resources` route, which is reponsible for the generation
    of the OpenApi JSON information schema. The `filter` configuration can be either a string or null.<br><br>
    The string passed to the `filter` configuration must match the fully qualified
    name of a class that extends the `LaravelSwagger\Processors\Operations\Filter`
    class. The referred custom class should implement methods that match properties
    to be used inside the OpenApi `x` property annotation in order to use them for
    validation. Those methods must return a boolean, either `true` or `false` based
    on the user custom validations.<br><br>
    When the method returns `true`, the tested `operation` will be discarded, and the
    opposite occurs when the method returns `false`.<br>
    If a `path` is left without `operations`, the `path` will be removed from
    the OpenAPI JSON information schema.<br>
    If the `filter` configuration is null or any other value type, no filter
    processing will be applied to the `operations`.
    ```
    Allowed value: String, NULL
    Optional: true
    Default: no default
    ```

## Examples
- <a name="info_example_1"><a>Info annotation using environment varibles
    ```
    <?php

    /**
    * @OA\Info(title=APP_NAME, version=APP_VERSION)
    */
    ```
