### For development with a base Laravel application
- Clone the repository
- Create the `app` directory inside the package's project workspace
- Run the command `composer create-project laravel/laravel --prefer-dist .` inside the created `app` folder
- Link the package as a local repository, so first add the following static repository mapping to the `app` laravel project's `composer.json` file
```
"repositories": [
    {
        "type": "path",
        "url": ".."
    }
],
```
- Run the command `composer require ardie.fernandes/laravel-swagger` to finally link the package as a _live_ composer dependency

##### Additional steps For UI development
- Run the command `npm i` inside the `src/swagger-ui` folder
- Next run the command `npm run dev`

##### For production build
- Run the command `npm run prod` inside the `src/swagger-ui` folder
