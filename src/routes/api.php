<?php declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use LaravelSwagger\Processors\FilterRestrictedAccessPaths;
use OpenApi\Analysis;
use OpenApi\Generator;

if (!config('swagger.enable', true)) {
    return;
}

$register = Route::prefix('api');

if (!empty(($middleware = config('swagger.middleware', null)))) {
    $register->middleware($middleware);
}

if (!empty(($namespace = config('swagger.namespace', null)))) {
    $register->namespace($namespace);
}

$register->group(function () {
    Route::view('/doc', 'vendor.laravel_swagger.index');
    Route::get('/resources', function () {
        define('APP_NAME', env('APP_NAME') ?? 'Laravel Swagger');
        define('APP_VERSION', env('APP_VERSION') ?? '0.1');

        $default = [app_path('Http/Controllers')];
        $path    = config('swagger.path', $default);
        $path    = is_string($path) ? [$path] : (is_array($path) ? $path : $default);

        Analysis::registerProcessor((new FilterRestrictedAccessPaths));
        $openapi = Generator::scan($path);

        echo $openapi->toJson();
    });
});
