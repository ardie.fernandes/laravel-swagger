<?php declare(strict_types=1);
namespace LaravelSwagger\Processors;

use OpenApi\Annotations\PathItem;
use OpenApi\Analysis;
use OpenApi\Generator;

/**
 * Filters paths with access restriction to not be shown in the documentation
 */
class FilterRestrictedAccessPaths
{
    /**
     * Processor filter handler
     *
     * @var \LaravelSwagger\Processors\Operations\Filter
     */
    private $handler;

    /**
     * Class invoke magic method
     *
     * @param \OpenApi\Analysis $analysis
     *
     * @return void
     */
    public function __invoke(Analysis $analysis) : void
    {
        if (!$this->enabled($analysis)) {
            return;
        }

        $paths   = [];
        $methods = array_filter(PathItem::$_nested, function ($item) {
            if (is_string($item)) {
                return $item;
            }
        });

        foreach ($analysis->openapi->paths as $annotation) {
            if (!$this->filter($annotation, $methods)) {
                $paths[] = $annotation;
            }
        }

        $analysis->openapi->paths = $paths;
    }

    /**
     * Runs a filter on the operation based on the it's "x properties" array.
     *
     * The "x properties" keys that match a method in the current handler
     * will be used to test the operation against the custom rules implemented
     * in the matched method.
     *
     * The custom methods must return a boolean; `true` will remove the operation
     * from the path, while `false` keeps the operation in its corresponding path.
     *
     * If a path is left without operations, then will be removed from the
     * OpenApi annotations store.
     *
     * @param \OpenApi\Annotations\PathItem $annotation
     * @param array $methods
     *
     * @return boolean
     */
    private function filter(PathItem $annotation, array $methods) : bool
    {
        $filter = true;

        foreach ($methods as $method) {
            if ($annotation->$method === Generator::UNDEFINED) {
                continue;
            }

            $operation = $annotation->$method;

            if (
                $operation->x !== Generator::UNDEFINED
                && $this->handler->filter($operation->x)
            ) {
                $annotation->$method = Generator::UNDEFINED;

                continue;
            }

            $filter = false;

            if (is_array($operation->x) && sizeof($operation->x) <= 0) {
                $operation->x = Generator::UNDEFINED;
            }
        }

        return $filter;
    }

    /**
     * Verifies if the processor has been properly enabled to run
     *
     * @param \OpenApi\Analysis $analysis
     *
     * @return void
     */
    private function enabled(Analysis $analysis)
    {
        if (
            $analysis->openapi->paths !== Generator::UNDEFINED &&
            !empty(($handler = config('swagger.filter', false))) &&
            is_string($handler) &&
            class_exists($handler) &&
            ($handler = new $handler) instanceof Operations\Filter
        ) {
            $this->handler = $handler;

            return true;
        }

        return false;
    }
}
