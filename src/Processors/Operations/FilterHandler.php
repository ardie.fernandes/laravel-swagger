<?php declare(strict_types=1);
namespace LaravelSwagger\Processors\Operations;

class FilterHandler extends Filter
{
    /**
     * Filters the operations that has been disabled
     *
     * @param string $value
     *
     * @return boolean
     */
    protected function disabled(string $value) : bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOL) === true;
    }
}
