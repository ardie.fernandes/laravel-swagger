<?php declare(strict_types=1);
namespace LaravelSwagger\Processors\Operations;

use Illuminate\Support\Str;

class Filter
{
    /**
     * Runs a filter on the operation based on the it's "x properties" array.
     *
     * The "x properties" keys that match a method in the current handler
     * will be used to test the operation against the custom rules implemented
     * in the matched method.
     *
     * The custom methods must return a boolean; `true` will remove the operation
     * from the path, while `false` keeps the operation in its corresponding path.
     *
     * If a path is left without operations, then will be removed from the
     * OpenApi annotations store.
     *
     * @param array $properties
     *
     * @return boolean
     */
    final public function filter(array &$properties) : bool
    {
        foreach ($properties as $key => $value) {
            $method = Str::camel(strtolower($key));

            if (method_exists($this, $method)) {
                unset($properties[$key]);

                if ($this->$method($value)) {
                    return true;
                }
            }
        }

        return false;
    }
}
