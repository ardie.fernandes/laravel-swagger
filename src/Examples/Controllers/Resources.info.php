<?php

/**
 * @OA\Get(
 *     path="/api/resources",
 *     x={"disabled"="true", "permission"="read:resources"},
 *     @OA\Response(response="200", description="An example resource"),
 *     security={
 *         {"api_auth": {"read:resources"}}
 *     }
 * )
 *
 * @OA\Post(
 *     path="/api/resources",
 *     x={"disabled"="false", "permission"="write:resources"},
 *     @OA\Response(response="200", description="An example resource"),
 *     security={
 *         {"api_auth": {"write:resources"}}
 *     }
 * )
 */
