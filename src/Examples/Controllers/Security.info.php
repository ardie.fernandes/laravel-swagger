<?php

/**
 * @OA\SecurityScheme(
 *     type="oauth2",
 *     name="api_auth",
 *     securityScheme="api_auth",
 *     @OA\Flow(
 *         flow="implicit",
 *         authorizationUrl="http://localhost:8080/api/doc",
 *         scopes={
 *             "write:resources": "modify resources in your account",
 *             "read:resources": "read your resources",
 *         }
 *     )
 * )
 */
