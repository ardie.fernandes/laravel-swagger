<?php declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | API Documentation Routes Enable Configuration
    |--------------------------------------------------------------------------
    |
    | Enables application access to API documentation routes based on
    | the APP_ENV environment configuration or the APIDOC_ENABLE environment
    | configuration
    |
    */

    'enable' => !in_array(
        env('APP_ENV', 'dev'),
        ['prod', 'production']
    ) || env('APIDOC_ENABLE', false),

    /*
    |--------------------------------------------------------------------------
    | API Documentation Path to Folder and Files to be Scanned
    |--------------------------------------------------------------------------
    |
    | The `path` configuration allows you to define the absolute path to the
    | folders and files to be scanned by the OpenAPI processors in search of
    | the supported annotations
    |
    | The `path` configuration can either be a string or an array of strings,
    | which in any case will reffer to the absolute path of the folders and
    | files to be scanned.
    |
    | Any other value type for the `path` configuration will set the OpenAPI
    | processors to scan the defaul folder app/Http/Controllers.
    |
    */

    'path' => null,
    // 'path' => [app_path()],
    // 'path' => app_path(),

    /*
    |--------------------------------------------------------------------------
    | API Documentation Routes Configurations
    |--------------------------------------------------------------------------
    |
    | Routes configurations allows you to define a `middleware` set to be used
    | when handling requests to the API documentation routes.
    |
    | As in any other Laravel routes configurations, the `middleware`
    | As in any other Laravel router configuration, the `middleware`
    | configuration can be either a string or an array of strings.
    |
    | The `middleware` configuration as a string, or an array of strings, can
    | either contain:
    | - The qualified class name, or an array of qualified classes names,
    | - An alias, or array of aliases (in both cases previously defined in the
    |   App\Http\Kernel class),
    | - A mix of both
    |
    | In any case the middlewares will be aplied in the defined order.
    |
    | If null, or any other value type, requests to the API documentation
    | routes will not pass through custom middleware handling.
    |
    */

    'middleware' => null,

    /*
    |--------------------------------------------------------------------------
    | API Dpcumentation Operations Filter Configuration
    |--------------------------------------------------------------------------
    |
    | The `filter` configuration allows the processor to filter the `operations`
    | returned by the `resources` route, which is reponsible for the generation
    | of the OpenApi JSON information schema.
    |
    | The `filter` configuration can be either a string or null.
    |
    | The string passed to the `filter` configuration must match the fully qualified
    | name of a class that extends the `LaravelSwagger\Processors\Operations\Filter`
    | class. The referred custom class should implement methods that match properties
    | to be used inside the OpenApi `x` property annotation in order to use them for
    | validation. Those methods must return a boolean, either `true` or `false` based
    | on the user custom validations.
    |
    | When the method returns `true`, the tested `operation` will be discarded, and the
    | opposite occurs when the method returns `false`.
    |
    | If a `path` is left without `operations`, the `path` will be removed from
    | the OpenAPI JSON information schema.
    |
    | If the `filter` configuration is null or any other value type, no filter
    | processing will be applied to the `operations`.
    |
    */

    'filter' => null,
    // 'filter' => LaravelSwagger\Processors\Operations\FilterHandler::class,

];
