<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') ?? 'Laravel Swagger' }}</title>
    <link rel="manifest" href="{{ asset('vendor/laravel_swagger/mix-manifest.json') }}">
</head>

<body>
    <div id="laravel_swagger_ui"></div>
    <script src="{{ asset('vendor/laravel_swagger/app.js') }}"></script>
</body>

</html>
