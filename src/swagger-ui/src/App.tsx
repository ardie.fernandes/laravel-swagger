import { FC } from 'react'
import SwaggerUI from 'swagger-ui-react'
import 'swagger-ui-react/swagger-ui.css'

const App: FC = () => (
  <div>
    {/* <div>Hello</div> */}
    <SwaggerUI url="/api/resources" />
  </div>
)

export default App
