const mix = require('laravel-mix')
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

mix
  .webpackConfig({
    plugins: [new CleanWebpackPlugin()],
  })
  .ts('src/index.js', 'app.js')
  .react()

if (!mix.inProduction()) {
  mix
    .webpackConfig({
      plugins: [
        new CopyPlugin({
          patterns: [
            {
              context: '../Examples',
              from: '**/*',
              to: path.resolve(__dirname, '../../app/app/Http/'),
            },
            {
              context: '../resources/views',
              from: '*',
              to: path.resolve(
                __dirname,
                '../../app/resources/views/vendor/laravel_swagger/'
              ),
            },
            {
              context: '../config',
              from: '*',
              to: path.resolve(__dirname, '../../app/config/'),
            },
          ],
        }),
      ],
    })
    .sourceMaps()
    .setPublicPath('./../../app/public/vendor/laravel_swagger')
} else {
  mix.setPublicPath('./../resources/js').version()
}
