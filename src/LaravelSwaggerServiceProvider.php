<?php declare(strict_types=1);
namespace LaravelSwagger;

use Illuminate\Support\ServiceProvider;

class LaravelSwaggerServiceProvider extends ServiceProvider
{
    /**
     * Provider boot.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');

        $this->publishes([
            __DIR__ . '/resources/views'    => resource_path('views/vendor/laravel_swagger'),
            __DIR__ . '/resources/js'       => public_path('vendor/laravel_swagger'),
            __DIR__ . '/config/swagger.php' => config_path('swagger.php'),
        ]);
    }
}
